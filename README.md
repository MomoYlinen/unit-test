# Unit testing

This project contains basic arithmetic functions with unit tests. There is also endpoint for every operation.

## Installation steps

1. Copy repository
2. Install dependencies
3. Start the app

## Terminal commands

```sh
git clone https://gitlab.com/MomoYlinen/node-task
cd node-task
npm install

```

## Arithmetic functions

Here is all the different arithmetic functions:

```
  sum: (a, b) => a + b,
  substraction: (a, b) => a - b,
  multiplication: (a, b) => a * b,
  random: () => Math.random() * (10 - 1) + 1,
  ZeroDivision: (a, b) => {
    if (b === 0) {
      throw new Error("Zero division!!!");
    }
    return a / b;
  },
```

## Test

There is five different unit tests, that can be run to make sure that the function returns correct results.

```
  it("Should return 4 when using sum function with a=3, b=1", () => {
    const result = mylib.sum(3, 1);
    expect(result).to.equal(4);
  });

  it("2 multiplied by 3 should equal 6", () => {
    expect(mylib.multiplication(2, 3)).to.equal(6);
  });

  it("10 multiplied by 6 should not equal 3", () => {
    expect(mylib.substraction(2, 3)).to.not.equal(3);
  });

  it("should throw an error when dividing by zero", () => {
    assert.throw(() => mylib.ZeroDivision(5, 0), Error, "Zero division!!!");
  });

  it("Should return random number between 1 and 10", () => {
    expect(mylib.random()).above(1).and.below(10);
  });
```

