module.exports = {
  sum: (a, b) => a + b,
  substraction: (a, b) => a - b,
  multiplication: (a, b) => a * b,
  random: () => Math.random() * (10 - 1) + 1,
  ZeroDivision: (a, b) => {
    if (b === 0) {
      throw new Error("Zero division!!!");
    }
    return a / b;
  },
};
