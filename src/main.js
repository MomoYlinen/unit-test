const express = require("express");
const app = express();
const port = 3000;
const mylib = require("./mylib");
app.get("/", (req, res) => {
  res.send("Hello world");
});

app.get("/add", (req, res) => {
  const a = parseInt(req.query.a);
  const b = parseInt(req.query.b);
  res.send(`${mylib.sum(a, b)}`);
});

app.get("/substration", (req, res) => {
  const a = parseInt(req.query.a);
  const b = parseInt(req.query.b);
  res.send(`${mylib.substraction(a, b)}`);
});

app.get("/random", (req, res) => {
  res.send(`${mylib.random()}`);
});

app.get("/multiply", (req, res) => {
  const a = parseInt(req.query.a);
  const b = parseInt(req.query.b);
  res.send(`${mylib.multiplication(a, b)}`);
});

app.get("/zero", (req, res) => {
  const a = parseInt(req.query.a);
  const b = parseInt(req.query.b);
  res.send(`${mylib.ZeroDivision(a, b)}`);
});

app.listen(port, () => {
  console.log(`Server: http://localhost:${port}`);
});
