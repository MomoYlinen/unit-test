const expect = require("chai").expect;
const assert = require("chai").assert;
const { arrayGen } = require("../src/mylib");
const mylib = require("../src/mylib");
describe("Unit testing mylib.js", () => {
  before(() => {
    console.log("This runs before all tests");
  });
  it("Should return 4 when using sum function with a=3, b=1", () => {
    const result = mylib.sum(3, 1);
    expect(result).to.equal(4);
  });

  it("2 multiplied by 3 should equal 6", () => {
    expect(mylib.multiplication(2, 3)).to.equal(6);
  });

  it("10 multiplied by 6 should not equal 3", () => {
    expect(mylib.substraction(2, 3)).to.not.equal(3);
  });

  it("should throw an error when dividing by zero", () => {
    assert.throw(() => mylib.ZeroDivision(5, 0), Error, "Zero division!!!");
  });

  it("Should return random number between 1 and 10", () => {
    expect(mylib.random()).above(1).and.below(10);
  });

  after(() => console.log("This should run after tests"));
});
